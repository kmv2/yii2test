<?php

namespace console\controllers;

use \Yii;
use yii\console\Controller;
use common\models\User;
use common\models\Album;
use \tebazil\yii2seeder\Seeder;

class SeedController extends Controller
{
    public function actionIndex()
    {

        $seeder = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();
        $dateGenerator = function(){ return time() - rand(1000, 10247); };
        $i = 0;
        $seeder->table('user')->columns([
            'id', //automatic pk
            'username' => function () use (&$i) {
                return 'user_'.$i++;
            }, //automatic fk
            'last_name'=> $faker->lastName,
            'first_name'=> $faker->firstName,
            'email'=> $faker->email,
            'created_at'=> $dateGenerator,
            'updated_at'=> $dateGenerator,
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->params['seeder.password']),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
        ])->rowQuantity(10);


        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();

        $seeder->refill();

        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function actionAlbums()
    {
        $seeder = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $colCfg = [
            'id',
            'user_id' => $faker->numberBetween(1,10),
            'title'=> $faker->sentence(3),
        ];

        $seeder->table('album')->columns($colCfg)->rowQuantity(100);

        $seeder->table('photo')->columns([
            'album_id' =>  $faker->numberBetween(1,100),
            'title' =>  $faker->sentence(5)
        ])->rowQuantity(1000);


        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();

        $seeder->refill();

        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }
}
