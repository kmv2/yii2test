<?php

use yii\db\Migration;

/**
 * Class m200521_101314_create_table_photos
 */
class m200521_101314_create_table_photos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('photo', [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer()->defaultValue('0')->notNull(),
            'title' => $this->string(255)->defaultValue('')->notNull(),
        ]);


        $this->createIndex(
            'album_id',
            'photo',
            'album_id'
        );

        $this->addForeignKey(
            'photo-album',
            'photo',
            'album_id',
            'album',
            'id',
            'CASCADE'
        );

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('photo');
        return true;
    }
}
