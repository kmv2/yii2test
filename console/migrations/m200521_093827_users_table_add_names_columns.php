<?php

use yii\db\Migration;

/**
 * Class m200521_093827_users_table_add_names_columns
 */
class m200521_093827_users_table_add_names_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%user}}', 'first_name', $this->string(50)->defaultValue(''));
        $this->addColumn('{{%user}}', 'last_name', $this->string(50)->defaultValue(''));
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%user}}', 'first_name');
        $this->dropColumn('{{%user}}', 'last_name');
        return true;
    }
}
