<?php

use yii\db\Migration;

/**
 * Class m200521_101259_create_table_albums
 */
class m200521_101259_create_table_albums extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('album', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue('0')->notNull(),
            'title' => $this->string(255)->defaultValue('')->notNull(),
        ]);

        $this->createIndex(
            'user_id',
            'album',
            'user_id'
        );

        $this->addForeignKey(
            'user_album',
            'album',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('album');
        return true;
    }
}
