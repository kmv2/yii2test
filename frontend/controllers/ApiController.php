<?php
namespace frontend\controllers;

use common\models\Album;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ApiController extends Controller
{

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['users', 'user', 'albums', 'album'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ]
            ],
        ];
    }

    public function actionUsers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->select(['id', 'first_name', 'last_name']),
            'pagination' => [
                'pageSize' => 2,
                'pageParam' => 'page'
            ]
        ]);

        return $dataProvider;
    }

    public function actionUser()
    {
        $user = User::findOne(['id' => Yii::$app->request->get('id')]);
        if(!$user)
        {
            throw new NotFoundHttpException('Not found');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'albums' => $user->getAlbums()->all(),
        ];
    }

    public function actionAlbums()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $dataProvider = new ActiveDataProvider([
            'query' => Album::find()->select(['id', 'title']),
            'pagination' => [
                'pageSize' => 5,
                'pageParam' => 'page'
            ]
        ]);
        return $dataProvider;
    }

    public function actionAlbum()
    {
        $album = Album::findOne(['id' => Yii::$app->request->get('id')]);
        if(!$album)
        {
            throw new NotFoundHttpException('Not found');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $r = [
            'id' => $album->id,
            'title' => $album->title,
            'photos' => []
        ];

        $photos = $album->getPhotos()->all();

        foreach ($photos as $photo)
        {
            $r['photos'][] = [
                'title' => $photo->title,
                'url' => $photo->url,
            ];
        }
        return $r;
    }

}
