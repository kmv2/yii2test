<?php


namespace common\models;


use yii\db\ActiveRecord;

class Album extends ActiveRecord
{
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['album_id' => 'id']);
    }
}
