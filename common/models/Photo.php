<?php


namespace common\models;


use yii\db\ActiveRecord;

/**
 * Class Photo
 * @package common\models
 */
class Photo extends ActiveRecord
{
    /**
     * Generate random url for static image
     * @return string
     */
    function getUrl()
    {
        return '/images/i'.rand(1, 8).'.png';
    }
}
